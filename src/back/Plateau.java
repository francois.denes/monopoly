package back;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import view.PlateauView;

/**
 * Plateau est la classe représentant un plateau du monopoly.
 *
 */
public class Plateau {
	/**
	 * La taille correspond au nombre de cases du plateau.
	 * Elle doit être égale à 20 ou 28.
	 * 
	 * @see Plateau#getTaille()
	 * @see Plateau#Plateau(int,int)
	 */
	private int taille;
	
	/**
	 * La valeur que l'on reçoit quand on passe la case départ.
	 * 
	 * @see Plateau#getCaseDepartPassage()
	 * @see Plateau#setCaseDepartPassage(int)
	 * @see Plateau#Plateau(int,int)
	 */
	private int caseDepartPassage;
	
	/**
	 * La valeur que l'on reçoit quand on passe la case départ.
	 * 
	 * @see Plateau#getCaseDepartArret()
	 * @see Plateau#setCaseDepartArret(int)
	 * @see Plateau#Plateau(int,int)
	 */
	private int caseDepartArret;
	
	
	/**
	 * L'id correspond au numéro de la case sur le plateau.
	 * 
	 * @see Plateau#getId()
	 * @see Plateau#setId(int)
	 */
	private int id;
	
	/**
	 * Le nom correspond au nom de la case sur le plateau.
	 * 
	 * @see Plateau#getNom()
	 * @see Plateau#setNom(String)
	 */
	private String nom;
	
	/**
	 * Le type correspond au type de la case sur le plateau.
	 * Cela permet de savoir à quelle classe elle appartient.
	 * 
	 * @see Plateau#getType()
	 * @see Plateau#setType(int)
	 */
	private int type;
	
	/**
	 * Le prix d'achat correspond au prix d'achat d'une case du plateau.
	 * 
	 * @see Plateau#getPrixAchat()
	 * @see Plateau#setPrixAchat(int)
	 */
	private int prixAchat;
	
	/**
	 * mat1 correspond au prix d'un matériel sur une matière. 
	 * Pour le deuxième matériel on reprend la même valeur.
	 * Et pour le dernier matériel on double cette valeur.
	 * 
	 * @see Plateau#getMat1()
	 * @see Plateau#setMat1(int)
	 */
	private int mat1;
	
	/**
	 * Loy0 correspond au loyer d'une case quand on a aucun matériel dessus.
	 * 
	 * @see Plateau#getLoy0()
	 * @see Plateau#setLoy0(int)
	 */
	private static int loy0;
	
	/**
	 * Loy1 correspond au loyer d'une case quand on a un matériel dessus.
	 * 
	 * @see Plateau#getLoy1()
	 * @see Plateau#setLoy1(int)
	 */
	private static int loy1;
	
	/**
	 * Loy2 correspond au loyer d'une case quand on a deux matériels dessus.
	 * 
	 * @see Plateau#getLoy2()
	 * @see Plateau#setLoy2(int)
	 */
	private static int loy2;
	
	/**
	 * Loy3 correspond au loyer d'une case quand on a trois matériels dessus.
	 * 
	 * @see Plateau#getLoy3()
	 * @see Plateau#setLoy3(int)
	 */
	private static int loy3;
	
	/**
	 * La couleur correspond à la couleur de la case sur le plateau.
	 * 
	 * @see Plateau#getCouleur()
	 * @see Plateau#setCouleur(String)
	 */
	private String couleur;
	
	/**
	 * La listePlateau correspond à la liste de toutes les cases sur le plateau.
	 * 
	 * @see Plateau#getListePlateau()
	 * @see Plateau#setListePlateau(ArrayList<Case>)
	 */
	private ArrayList<Case> listePlateau;
	
	
	
	
	private PlateauView view;
	

	/** Constructeur d'un plateau
	 * Quand la valeur entrée n'est pas bonne le plateau est automatiquement initialisé à 20 cases. 
	 * On initialise la liste des cases du plateau dans la liste.
	 * On initialise aussi le nombre de ligne dans le fichier csv.s
	 * 
	 * @param taille 
	 * 		Le nombre de cases du plateau à créer, 20 ou 28.
	 * @param caseDepPassage
	 * 		l'argent que l'on recupère quand on passe la case départ
	 * 
	 * @param caseDepArret
	 * 		l'argent que l'on recupère quand on passe la case départ
	 */
	public Plateau(int taille, int caseDepPassage, int caseDepArret) {
		this.taille=taille==28?taille:20;
		this.caseDepartPassage = caseDepPassage;
		this.caseDepartArret = caseDepArret;
		this.listePlateau=new ArrayList<>();
		initialisationListe(taille);
		view=new PlateauView(this);
	}
	
	public PlateauView getView() {
		return view;
	}


	/**	
	 * Récupérer la taille du plateau
	 * 
	 * @return Un entier correspondant à la taille du plateau, il s'agit du nombre de cases du plateau.
	 */
	public int getTaille() {
		return taille;
	}

	/**	
	 * Récupérer la valeur de la case départ.
	 * 
	 * @return Un entier correspondant à la valeur au passage de la case départ.
	 */
	public int getCaseDepartPassage() {
		return caseDepartPassage;
	}

	/**	
	 * Méthode de modification de la valeur de la case départ.
	 * 
	 * @param caseDepartPassage
	 * 		La nouvelle valeur de la case départ.
	 */
	public void setCaseDepartPassage(int caseDepartPassage) {
		this.caseDepartPassage = caseDepartPassage;
	}
	
	/**	
	 * Récupérer la valeur de la case départ.
	 * 
	 * @return Un entier correspondant à la valeur à l'arrêt sur la case départ.
	 */
	public int getCaseDepartArret() {
		return caseDepartArret;
	}

	/**	
	 * Méthode de modification de la valeur de la case départ.
	 * 
	 * @param caseDepartArret
	 * 		La nouvelle valeur de la case départ.
	 */
	public void setCaseDepartArret(int caseDepartArret) {
		this.caseDepartPassage = caseDepartArret;
	}
	

	/**	
	 * Récupérer la valeur de l'id de la case.
	 * 
	 * @return Un entier correspondant à la valeur de l'id.
	 */
	public int getId() {
		return id;
	}


	/**	
	 * Méthode de modification de l'id.
	 * 
	 * @param id
	 * 		La nouvelle valeur de l'id.
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**	
	 * Récupérer la valeur du nom de la case.
	 * 
	 * @return Une chaine de charatères correspondant au nom de la case.
	 */
	public String getNom() {
		return nom;
	}


	/**	
	 * Méthode de modification du nom de la case.
	 * 
	 * @param nom
	 * 		Le nouveau nom.
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**	
	 * Récupérer la valeur du type de la case.
	 * 
	 * @return Un entier correspondant au type de la case.
	 */
	public int getType() {
		return type;
	}


	/**	
	 * Méthode de modification du type de la case.
	 * 
	 * @param type
	 * 		La nouvelle valeur du type.
	 */
	public void setType(int type) {
		this.type = type;
	}

	
	/**	
	 * Récupérer la valeur du prix d'achat.
	 * 
	 * @return Un entier correspondant au prix d'achat.
	 */
	public int getPrixAchat() {
		return prixAchat;
	}


	/**	
	 * Méthode de modification du prix d'achat.
	 * 
	 * @param prixAchat caseDepartPassage
	 * 		La nouvelle valeur du prix d'achat.
	 */
	public void setPrixAchat(int prixAchat) {
		this.prixAchat = prixAchat;
	}


	/**	
	 * Récupérer la valeur pour l'achat d'un matériel.
	 * 
	 * @return Un entier correspondant la valeur d'achat d'un matériel.
	 */
	public int getMat1() {
		return mat1;
	}


	/**	
	 * Méthode de modification de la valeur d'un achat de matériel.
	 * 
	 * @param mat1
	 * 		La nouvelle valeur d'achat d'un matériel.
	 */
	public void setMat1(int mat1) {
		this.mat1 = mat1;
	}


	/**	
	 * Récupérer la valeur du premier loyer, sans matériel.
	 * 
	 * @return Un entier correspondant à la valeur du permier loyer.
	 */
	public static int getLoy0() {
		return loy0;
	}


	/**	
	 * Methode de modification de la valeur du premier loyer, avec un matériel.
	 * 
	 * @param loy0
	 * 		La nouvelle valeur du premier loyer.
	 */
	public static void setLoy0(int loy0) {
		Plateau.loy0 = loy0;
	}


	/**	
	 * Récupérer la valeur du deuxième loyer, avec un matériel.
	 * 
	 * @return Un entier correspondant à la valeur du deuxième loyer.
	 */
	public static int getLoy1() {
		return loy1;
	}


	/**	
	 * Methode de modification de la valeur du deuxième loyer, avec un matériel.
	 * 
	 * @param loy1
	 * 		La nouvelle valeur du deuxième loyer.
	 */
	public static void setLoy1(int loy1) {
		Plateau.loy1 = loy1;
	}


	/**	
	 * Récupérer la valeur du troisième loyer, avec deux matériels.
	 * 
	 * @return Un entier correspondant à la valeur du troisième loyer.
	 */
	public static int getLoy2() {
		return loy2;
	}

	
	/**	
	 * Methode de modification de la valeur du troisième loyer, avec deux matériels.
	 * 
	 * @param loy2 caseDepartPassage
	 * 		La nouvelle valeur du troisième loyer.
	 */
	public static void setLoy2(int loy2) {
		Plateau.loy2 = loy2;
	}


	/**	
	 * Récupérer la valeur du quatrième loyer, avec trois matériels.
	 * 
	 * @return Un entier correspondant à la valeur du quatrième loyer.
	 */
	public static int getLoy3() {
		return loy3;
	}

	
	/**	
	 * Methode de modification de la valeur du quatrième loyer, avec trois matériels.
	 * 
	 * @param loy3
	 * 		La nouvelle valeur du quatrième loyer.
	 */
	public static void setLoy3(int loy3) {
		Plateau.loy3 = loy3;
	}

	
	/**	
	 * Récupérer la valeur de la couleur.
	 * 
	 * @return Une chaine de charactères correspondant à la couleur de la case.
	 */
	public String getCouleur() {
		return couleur;
	}


	/**	
	 * Methode de modification de la couleur de la case.
	 * 
	 * @param couleur
	 * 		La nouvelle valeur de la couleur.
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	
	/**	
	 * Récupérer la liste des cases du plateau.
	 * 
	 * @return Une ArrayList de Case correspondant aux cases du plateau.
	 */
	public ArrayList<Case> getListePlateau() {
		return listePlateau;
	}


	/**	
	 * Methode de modification des cases du plateau.
	 * 
	 * @param listePlateau
	 * 		La nouvelle ArrayList de Case.
	 */
	public void setListePlateau(ArrayList<Case> listePlateau) {
		this.listePlateau = listePlateau;
	}

	public Case getCase(String nom) {
		for(Case c : listePlateau)if(c.getNom().equals(nom))return c;
		return getInteractif();
	}
	
	private Case getInteractif() {
		for(Case c : listePlateau)if(c instanceof Interactif)return c;
		return null;
	}
	
	public int getNbMateriels() {
		int total=0;
		for(Case c : listePlateau) {
			if(c instanceof Salle && ((Salle) c).estPossedee()) {
				total+=((Salle) c).getNbMateriels();
			}
		}
		return total;
	}
	
	public void modifierLoyers(boolean modif) {
		for(Case c : listePlateau) {
			if(c instanceof Salle) {
				((Salle) c).setCoutLoyer0((int)(((Salle) c).getCoutLoyer0()*(1+getNbMateriels()*0.001*(modif?1:-1))));
				((Salle) c).setCoutLoyer1((int)(((Salle) c).getCoutLoyer1()*(1+getNbMateriels()*0.001*(modif?1:-1))));
				((Salle) c).setCoutLoyer2((int)(((Salle) c).getCoutLoyer2()*(1+getNbMateriels()*0.001*(modif?1:-1))));
				((Salle) c).setCoutLoyer3((int)(((Salle) c).getCoutLoyer3()*(1+getNbMateriels()*0.001*(modif?1:-1))));
			}
		}
	}
	
	public boolean toutesCasesAchetees() {
		for(Case c : listePlateau) {
			if(c instanceof Propriete&&!((Propriete) c).estPossedee())return false;
		}
		return true;
	}

	/**
	 * 	
	 * Récupérer les informations des cases du plateau. On lit un fichier csv et on récupère les informations 
	 * des cases.
	 * 
	 * @param num
	 * 		le nombre de lignes dans le fichier csv.
	 */
	private void getInfoCase(int num){
		int compteur = 0;
		try {
			File fileDir = new File(taille==28?"csv/casesGrandPlateau.csv":"csv/casesPetitPlateau.csv");
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF8"));
			
			for(int i=1;i<num+1;i++) {
				in.readLine();
				compteur++;
			}
			if (compteur!=0) {
				String ligne = in.readLine();
				String[] data = ligne.split(";");
				
				//System.out.println(ligne);
				
				this.id = Integer.parseInt(data[0]);
				this.nom = data[1];
				this.type = Integer.parseInt(data[2]);
				this.prixAchat = Integer.parseInt(data[3]);
				this.mat1 = Integer.parseInt(data[4]);
				Plateau.loy0 = Integer.parseInt(data[5]);
				Plateau.loy1 = Integer.parseInt(data[6]);
				Plateau.loy2 = Integer.parseInt(data[7]);
				Plateau.loy3 = Integer.parseInt(data[8]);
				this.couleur = data[9];
							
				in.close();
			}else {
				System.out.println("1e ligne");
			}
		}
		catch(IOException e) {
			System.out.println(e);
		}
	
	}
	
	
	/**
	 * 	
	 * Initialise la liste des cases du plateau. Chaque case a un type different et on initialise à la bonne classe. 
	 * 
	 * @param bornMax
	 * 		nombre de lignes du csv a mettre dans la liste
	 */
	private void initialisationListe(int borneMax) {
		for (int i=1;i<=borneMax;i++) { //parcours le fichier
			
			this.getInfoCase(i); //attribu les bonnes valeurs aux variables
			
			if (this.type ==1) {
				listePlateau.add(new Depart(this.nom, this.id, this.caseDepartPassage, this.caseDepartArret, this.couleur ));
			}
			else if (this.type ==2) {
				listePlateau.add(new Salle(this.nom, this.id, this.couleur, this.prixAchat,this.mat1,Plateau.loy0,Plateau.loy1,Plateau.loy2,Plateau.loy3));
			}
			else if (this.type ==3) {
				listePlateau.add(new Chance(this.nom,this.id, this.couleur));
			}
			else if (this.type ==4) {
				listePlateau.add(new Interactif(this.nom,this.id, this.couleur));
			}
			else if (this.type ==5) {
				listePlateau.add(new SalleDeDS(this.nom,this.id, this.couleur,this.prixAchat));
			}
			else if (this.type ==6) {
				listePlateau.add(new TaxeDeLuxe(this.nom, this.id, this.couleur));
			}
			else if (this.type ==7) {
				listePlateau.add(new CentreCommercial(this.nom, this.id, this.couleur,this.prixAchat, Plateau.loy0, Plateau.loy1, Plateau.loy2, Plateau.loy3));
			}
			else if (this.type ==8) {
				listePlateau.add(new Service(this.nom,this.id, this.couleur, this.prixAchat));
			}
			else {
				System.out.println("Erreur, case pas dans le plateau");;
			}			
		}
	}


	
}
