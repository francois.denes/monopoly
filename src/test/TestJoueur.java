package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;



public class TestJoueur {

	   @Test
	   public void testGagnerArgentType0() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",100,false);
	       
	       j.gagnerArgent(100,0);
	       
	       assertEquals(j.getArgentBis(),200);
	       assertEquals(j.getStats().getGagne(),100);
	   }
	   
	   @Test
	   public void testGagnerArgentType1() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",100,false);
	       
	       j.gagnerArgent(100,1);
	       
	       assertEquals(j.getArgentBis(),200);
	       assertEquals(j.getStats().getGagne(),100);
	       assertEquals(j.getStats().getGagneLocation(),100);
	   }
	   
	   @Test
	   public void testGagnerArgentType2() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",100,false);
	       
	       j.gagnerArgent(100,2);
	       
	       assertEquals(j.getArgentBis(),200);
	       assertEquals(j.getStats().getGagne(),100);
	       assertEquals(j.getStats().getGagneChances(),100);
	   }
	   
	   @Test
	   public void testGagnerArgentTypeNonDef() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",100,false);
	       
	       j.gagnerArgent(100,5);
	       
	     //on attend une erreur 
	   }
	   
	   @Test
	   public void testPerdreArgentType0() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",200,false);
	       
	       j.perdreArgent(100,0);
	       
	       assertEquals(j.getArgentBis(),100);
	       assertEquals(j.getStats().getPerdu(),100);
	   }
	   
	   @Test
	   public void testPerdreArgentType1() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",200,false);
	       
	       j.perdreArgent(100,1);
	       
	       assertEquals(j.getArgentBis(),100);
	       assertEquals(j.getStats().getPerdu(),100);
	       assertEquals(j.getStats().getPerduLocation(),100);
	   }
	   
	   @Test
	   public void testPerdreArgentType2() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",200,false);
	       
	       j.perdreArgent(100,2);
	       
	       assertEquals(j.getArgentBis(),100);
	       assertEquals(j.getStats().getPerdu(),100);
	       assertEquals(j.getStats().getPerduChances(),100);
	   }
	   
	   @Test
	   public void testPerdreArgentTypeNonDef() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",200,false);
	       
	       j.gagnerArgent(100,5);
	       
	       //on attend une erreur 
	       
	   }
	   
	   @Test
	   public void testLancerDe() {
	       JoueurTest j = new JoueurTest("test1","#c0c0c0",200,false);
	        
	       j.lancerDe();
	       
	       assertTrue(0<=j.getDe1()&&j.getDe1()<5);
	       assertTrue(0<j.getDe1()&&j.getDe2()<5);
	   }
	   
	   
	   
}
