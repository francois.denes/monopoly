package test;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import back.CarteChance;
import junit.framework.TestCase;

public class TestCartesChances extends TestCase {
	private final static String FILE_NAME = "csv/cartesChances.csv";

	   //test d'ouverture du fichier
	   @Test
	   public void testGetResource() {
	       // Param
	       final String fileName = FILE_NAME;

	       // Conversion
	       final File f = new File("");
	       final String dossierPath = f.getAbsolutePath()+File.separator+ fileName;
	       final String completeFileName = dossierPath;
	       File file = new File(completeFileName);
	     
	       // Test
	       // On sait que le fichier existe bien puisque c'est avec lui qu'on travaille depuis le début.
	       assertTrue(file.exists());
	   }
	   
	   //test du nombre de lignes
	   @Test
	   public void testReadFile() throws IOException {
		   CarteChance c = new CarteChance();
	       // Result
	       final int nombreLigne = 22;

	       // Appel
	       int nb = c.getNbCarte();

	       // Test
	       Assert.assertEquals(nombreLigne, nb+1);
	   }

}
