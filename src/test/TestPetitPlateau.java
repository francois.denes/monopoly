package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;

public class TestPetitPlateau extends TestCase {
	private final static String FILE_NAME = "csv/casesPetitPlateau.csv";

	   //test d'ouverture du fichier
	   @Test
	   public void testGetResource() {
	       // Param
	       final String fileName = FILE_NAME;

	       // Conversion
	       final File f = new File("");
	       final String dossierPath = f.getAbsolutePath()+File.separator+ fileName;
	       final String completeFileName = dossierPath;
	       File file = new File(completeFileName);
	     
	       // Test
	       // On sait que le fichier existe bien puisque c'est avec lui qu'on travaille depuis le début.
	       assertTrue(file.exists());
	   }
	   
	   //test du nombre de lignes
	   @Test
	   public void testReadFile() throws IOException {
		   int compteur = 0;
		   
		   // Param
	       File file = new File(FILE_NAME);
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			
			
			
	       // Result
	       final int nombreLigne = 21;

	       // Appel
	       String ligne = in.readLine();
	       while(ligne!=null) {
				ligne = in.readLine();
				compteur++;
			}

	       // Test
	       Assert.assertEquals(nombreLigne, compteur);
	   }

}
